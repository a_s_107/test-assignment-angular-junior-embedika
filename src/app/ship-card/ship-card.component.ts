import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApolloQueryResult } from '@apollo/client/core';
import { Subscription } from 'rxjs';

import { ShipService } from '../ship.service';
import { ExtendedShip, ShipQuery } from '../types';

@Component({
  selector: 'app-ship-card',
  templateUrl: './ship-card.component.html',
  styleUrls: ['./ship-card.component.css'],
})
export class ShipCardComponent implements OnInit, OnDestroy {
  private _queryShipSubscription$!: Subscription;

  ship: ExtendedShip | undefined;
  isLoading = true;

  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _shipService: ShipService
  ) {}

  ngOnInit(): void {
    const routeParams = this._route.snapshot.paramMap;
    const shipId = routeParams.get('shipId')?.toUpperCase();

    if (shipId !== undefined) {
      this._queryShipSubscription$ = this._shipService
        .getShip(shipId)
        .subscribe((result: ApolloQueryResult<ShipQuery>) => {
          this.ship = result?.data?.ship;
          this.isLoading = result.loading;
        });
    }
  }

  navigateBack() {
    const returnUrl = this._route.snapshot.queryParamMap.get('returnUrl');
    if (returnUrl) {
      this._router.navigateByUrl(returnUrl);
    } else {
      this._router.navigate(['ships']);
    }
  }

  ngOnDestroy(): void {
    this._queryShipSubscription$.unsubscribe();
  }
}
