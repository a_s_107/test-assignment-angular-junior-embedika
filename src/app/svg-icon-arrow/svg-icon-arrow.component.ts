import { Component, OnInit, Input } from '@angular/core';

type Direction = 'left' | 'right';

@Component({
  selector: 'app-svg-icon-arrow',
  templateUrl: './svg-icon-arrow.component.svg',
  styleUrls: ['./svg-icon-arrow.component.css'],
})
export class SvgIconArrowComponent implements OnInit {
  @Input() direction!: Direction;
  @Input() disabled: boolean = false;

  colors = {
    regular: '#347CFF',
    disabled: '#3C474C',
  };

  constructor() {}

  ngOnInit(): void {}
}
