import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ShipCardComponent } from './ship-card/ship-card.component';
import { ShipListComponent } from './ship-list/ship-list.component';

const routes: Routes = [
  { path: 'ships', component: ShipListComponent },
  { path: 'ships/:shipId', component: ShipCardComponent },
  { path: '', redirectTo: '/ships', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
