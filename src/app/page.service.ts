import { Injectable } from '@angular/core';
import { forkJoin, Observable, of } from 'rxjs';
import { first, map, mergeMap } from 'rxjs/operators';

import {
  ArgsComplementPage,
  ArgsCreatePage,
  ArgsCreatePageBasedOnShipRequest,
  Filters,
  Page,
  QueryArgs,
  Ship,
  ShipRequest,
} from './types';
import { ShipService } from './ship.service';

@Injectable({
  providedIn: 'root',
})
export class PageService {
  private _pages$: Observable<Page[]> | undefined;

  constructor(private _shipService: ShipService) {}

  createPageBasedOnShipRequest({
    querysArgs = [],
    shipRequest,
    limit,
    offset,
    size,
  }: ArgsCreatePageBasedOnShipRequest): Page {
    return {
      querysArgs: [
        ...querysArgs,
        {
          ...shipRequest.args,
          limit,
          offset,
        },
      ],
      size,
    };
  }

  complementPage({
    page,
    shipRequest,
    pageSize,
    shipCount,
  }: ArgsComplementPage): { modifiedPage: Page; newShipCount: number } {
    let modifiedPage: Page;

    if (page.size + shipCount > pageSize) {
      modifiedPage = this.createPageBasedOnShipRequest({
        querysArgs: page.querysArgs,
        shipRequest,
        limit: pageSize - page.size,
        offset: shipRequest.count - shipCount,
        size: pageSize,
      });
      shipCount -= pageSize - page.size;
    } else {
      modifiedPage = this.createPageBasedOnShipRequest({
        querysArgs: page.querysArgs,
        shipRequest,
        limit: shipCount,
        offset: shipRequest.count - shipCount,
        size: page.size + shipCount,
      });
      shipCount = 0;
    }

    return { modifiedPage, newShipCount: shipCount };
  }

  createPage({ shipRequest, pageSize, shipCount }: ArgsCreatePage): {
    page: Page;
    newShipCount: number;
  } {
    let page: Page;

    if (shipCount < pageSize) {
      page = this.createPageBasedOnShipRequest({
        shipRequest,
        limit: shipCount,
        offset: shipRequest.count - shipCount,
        size: shipCount,
      });
      shipCount = 0;
    } else {
      page = this.createPageBasedOnShipRequest({
        shipRequest,
        limit: pageSize,
        offset: shipRequest.count - shipCount,
        size: pageSize,
      });
      shipCount -= pageSize;
    }

    return { page, newShipCount: shipCount };
  }

  getPages({
    shipRequests,
    pageSize,
  }: {
    shipRequests: ShipRequest[];
    pageSize: number;
  }): Page[] {
    return shipRequests.reduce((acc: Page[], shipRequest: ShipRequest) => {
      let shipCount: number = shipRequest.count;
      let pages: Page[] = [...acc];

      while (shipCount !== 0) {
        const lastPage: Page = pages[pages.length - 1];

        if (lastPage !== undefined && lastPage.size !== pageSize) {
          const { modifiedPage: modifiedLastPage, newShipCount } =
            this.complementPage({
              page: lastPage,
              shipRequest,
              pageSize,
              shipCount,
            });
          shipCount = newShipCount;
          pages = [...pages.slice(0, -1), modifiedLastPage];
        } else {
          const { page, newShipCount } = this.createPage({
            shipRequest,
            pageSize,
            shipCount,
          });
          shipCount = newShipCount;
          pages = [...pages, page];
        }
      }

      return pages;
    }, []);
  }

  setPagesBasedOnMultipleRequests({
    pageSize,
    argsForRequests,
  }: {
    pageSize: number;
    argsForRequests: QueryArgs[];
  }): void {
    const arrRequestShipCount$: Observable<number>[] = argsForRequests.map(
      (argsForReq) => this._shipService.getTotalCountShip(argsForReq)
    );
    this._pages$ = forkJoin(arrRequestShipCount$).pipe(
      first(),
      map((arrShipCount: number[]): ShipRequest[] =>
        arrShipCount.map(
          (count, index): ShipRequest => ({
            args: argsForRequests[index],
            count,
          })
        )
      ),
      map((shipRequests: ShipRequest[]): Page[] =>
        this.getPages({ shipRequests, pageSize })
      )
    );
  }

  setPagesBasedOnFilters({
    filters,
    pageSize,
  }: {
    filters: Filters;
    pageSize: number;
  }) {
    if (filters.ports === undefined || filters.ports.length === 1) {
      const port: string | undefined =
        filters.ports === undefined ? undefined : filters.ports[0];
      const argsForRequest: QueryArgs = {
        name: filters.name,
        port,
        type: filters.type,
      };
      this.setPagesBasedOnMultipleRequests({
        pageSize,
        argsForRequests: [argsForRequest],
      });
    } else {
      const argsForRequests: QueryArgs[] = filters.ports.map((port) => {
        return { name: filters.name, port, type: filters.type };
      });

      this.setPagesBasedOnMultipleRequests({
        pageSize,
        argsForRequests,
      });
    }

    return this._pages$;
  }

  getPage(numPage: number): Observable<Ship[]> | undefined {
    return this._pages$?.pipe(
      mergeMap((pages: Page[]) => {
        if (pages.length > 0) {
          return forkJoin(
            pages[numPage].querysArgs.map((args) =>
              this._shipService.getShips(args)
            )
          ).pipe(
            first(),
            map((result) =>
              result.reduce(
                (acc: Ship[], item) => [...acc, ...item.data.ships],
                []
              )
            )
          );
        } else {
          return of([]);
        }
      })
    );
  }
}
