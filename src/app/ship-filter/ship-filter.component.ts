import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Observable } from 'rxjs';

import { ShipService } from '../ship.service';
import { Filters, ShipName } from '../types';

@Component({
  selector: 'app-ship-filter',
  templateUrl: './ship-filter.component.html',
  styleUrls: ['./ship-filter.component.css'],
})
export class ShipFilterComponent implements OnInit {
  @Input() filters!: Filters;
  @Output() updateShipsByFilters = new EventEmitter<Filters>();

  nameOptions$!: Observable<ShipName[]>;
  portOptions$!: Observable<string[]>;
  typeOptions$!: Observable<string[]>;

  constructor(private _shipService: ShipService) {}

  ngOnInit(): void {
    this.nameOptions$ = this._shipService.getShipNames();
    this.typeOptions$ = this._shipService.getShipTypes();
    this.portOptions$ = this._shipService.getShipPorts();
  }

  updateName(name: string): void {
    this.updateShipsByFilters.emit({
      ...this.filters,
      name,
    });
  }

  updateType(type: string): void {
    this.updateShipsByFilters.emit({
      ...this.filters,
      type,
    });
  }

  updatePort(ports: string[] | undefined): void {
    this.updateShipsByFilters.emit({
      ...this.filters,
      ports,
    });
  }
}
