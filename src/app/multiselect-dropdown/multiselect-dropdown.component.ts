import {
  Component,
  ElementRef,
  EventEmitter,
  HostListener,
  Input,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';
import { Observable, Subscription } from 'rxjs';

@Component({
  selector: 'app-multiselect-dropdown',
  templateUrl: './multiselect-dropdown.component.html',
  styleUrls: ['./multiselect-dropdown.component.css'],
})
export class MultiselectDropdownComponent implements OnInit, OnDestroy {
  @Input() value: string[] | undefined;
  @Input() options$!: Observable<string[]>;
  @Output() updatePort = new EventEmitter<string[]>();

  private _portsChangesSubscription$: Subscription | undefined;

  numOfSelectedItems: number = 0;
  multiselect = this._fb.group({});
  isOpen: boolean = false;
  portCount: number | undefined;

  constructor(private _fb: FormBuilder, private _el: ElementRef) {}

  ngOnInit(): void {
    this.options$.subscribe((portNames) => {
      if (this.value !== undefined && this.value.length > 0) {
        portNames.forEach((name) => {
          const selected = this.value?.includes(name);
          this.multiselect.addControl(name, new FormControl(selected));
        });
      } else {
        portNames.forEach((name) => {
          this.multiselect.addControl(name, new FormControl(true));
        });
      }

      this.portCount = portNames.length;
    });
    this.setNumOfSelectedItems();
    this._portsChangesSubscription$ = this.multiselect.valueChanges.subscribe(
      (data) => {
        const selectedItems = Object.entries(data)
          .filter((item) => item[1])
          .map((item) => item[0]);
        this.numOfSelectedItems = selectedItems.length;
        this.updatePort.emit(
          this.portCount === this.numOfSelectedItems ? undefined : selectedItems
        );
      }
    );
  }

  @HostListener('document:click', ['$event'])
  onClick(event: Event) {
    if (!this._el.nativeElement.contains(event.target)) {
      this.isOpen = false;
    }
  }

  setNumOfSelectedItems(): void {
    this.numOfSelectedItems = Object.values(this.multiselect.value).reduce(
      (acc: number, option) => {
        return acc + Number(option);
      },
      0
    );
  }

  getNumOfSelectedItemsToString(): string {
    if (this.numOfSelectedItems === 0) {
      return '';
    }

    return this.numOfSelectedItems === 1
      ? 'Выбран 1'
      : `Выбраны ${this.numOfSelectedItems}`;
  }

  activeDropdown(): void {
    this.isOpen = !this.isOpen;
  }

  close(): void {
    this.isOpen = false;
  }

  ngOnDestroy(): void {
    this._portsChangesSubscription$?.unsubscribe();
  }
}
