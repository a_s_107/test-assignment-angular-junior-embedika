import { Injectable } from '@angular/core';
import { ApolloQueryResult } from '@apollo/client/core';
import { Apollo } from 'apollo-angular';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';

import {
  SHIP_QUERY,
  SHIPS_QUERY,
  TOTAL_COUNT_SHIP_QUERY,
  SHIP_NAMES,
} from './graphql';
import {
  QueryArgs,
  ShipName,
  ShipNamesQuery,
  ShipQuery,
  ShipsQuery,
  TotalCountQuery,
} from './types';

@Injectable({
  providedIn: 'root',
})
export class ShipService {
  constructor(private _apollo: Apollo) {}

  getShips(args: QueryArgs): Observable<ApolloQueryResult<ShipsQuery>> {
    return this._apollo.query<ShipsQuery>({
      query: SHIPS_QUERY,
      variables: args,
    });
  }

  getTotalCountShip(args?: QueryArgs): Observable<number> {
    return this._apollo
      .query<TotalCountQuery>({
        query: TOTAL_COUNT_SHIP_QUERY,
        variables: args,
      })
      .pipe(map((result) => result.data.shipsResult.result.totalCount));
  }

  getShip(id: string): Observable<ApolloQueryResult<ShipQuery>> {
    return this._apollo.query<ShipQuery>({
      query: SHIP_QUERY,
      variables: { id },
    });
  }

  getShipNames(): Observable<ShipName[]> {
    return this._apollo
      .query<ShipNamesQuery>({
        query: SHIP_NAMES,
      })
      .pipe(map((result) => result.data.ships));
  }

  getShipTypes(): Observable<string[]> {
    return of(['Barge', 'Cargo', 'High Speed Craft', 'Tug']);
  }

  getShipPorts(): Observable<string[]> {
    return of(['Port Canaveral', 'Port of Los Angeles', 'Fort Lauderdale']);
  }
}
