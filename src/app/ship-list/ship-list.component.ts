import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { Ship, Filters } from '../types';
import { PageService } from '../page.service';

@Component({
  selector: 'app-ship-list',
  templateUrl: './ship-list.component.html',
  styleUrls: ['./ship-list.component.css'],
})
export class ShipListComponent implements OnInit, OnDestroy {
  private _queryShipsSubscription$: Subscription | undefined;
  private _queryPageCountSubscription$: Subscription | undefined;

  ships: Ship[] = [];
  pageSize: number = 5;
  isLoading = true;
  currentPage!: number;
  pageCount!: number;
  filters!: Filters;

  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _pageService: PageService
  ) {}

  ngOnInit(): void {
    const routeQueryParams: ParamMap = this._route.snapshot.queryParamMap;
    const page: number = Number(routeQueryParams.get('page'));
    const currentPage: number = page ? page : 1;
    const filters: Filters = this.getFilterParamsInRoute(routeQueryParams);

    this.updateShipsByFilters(filters, currentPage);
  }

  getFilterParamsInRoute(paramMap: ParamMap) {
    const name = paramMap.get('name');
    const ports = paramMap.getAll('ports');
    const type = paramMap.get('type');

    return {
      name: name === null ? '' : name,
      ports: ports.length === 0 ? undefined : ports,
      type: type === null ? '' : type,
    };
  }

  getActiveFilters(): Filters {
    return Object.entries(this.filters)
      .filter(([_, value]) => value != '')
      .reduce((acc, [key, value]) => {
        return {
          ...acc,
          [key]: value,
        };
      }, {});
  }

  setUrlBasedOnFilters(filters: Filters): void {
    this._router.navigate(['ships'], {
      queryParams: {
        page: this.currentPage,
        ...filters,
      },
    });
  }

  updatePageShips(page: number): void {
    this.currentPage = page;
    this._queryShipsSubscription$ = this._pageService
      .getPage(this.currentPage - 1)
      ?.subscribe((ships) => {
        this.ships = ships;
        this.isLoading = false;
      });
  }

  updateShipsByFilters(filters: Filters, numPage?: number): void {
    this.filters = filters;
    this.currentPage = numPage !== undefined ? numPage : 1;
    const params: Filters = this.getActiveFilters();
    this.setUrlBasedOnFilters(params);
    this._queryPageCountSubscription$ = this._pageService
      .setPagesBasedOnFilters({
        filters: params,
        pageSize: this.pageSize,
      })
      ?.subscribe((pages) => {
        this.pageCount = pages.length;
      });
    this.updatePageShips(this.currentPage);
  }

  getUrl(): string {
    return this._router.url;
  }

  ngOnDestroy(): void {
    this._queryShipsSubscription$?.unsubscribe();
    this._queryPageCountSubscription$?.unsubscribe();
  }
}
