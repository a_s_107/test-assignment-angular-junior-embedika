import {
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable, Subscription } from 'rxjs';

@Component({
  selector: 'app-single-select',
  templateUrl: './single-select.component.html',
  styleUrls: ['./single-select.component.css'],
})
export class SingleSelectComponent implements OnInit, OnDestroy {
  @Input() value: string | undefined;
  @Input() options$!: Observable<string[]>;
  @Output() updateType = new EventEmitter<string>();

  private _typeChangesSubscription$: Subscription | undefined;

  type = new FormControl('');

  constructor() {}

  ngOnInit(): void {
    this.type.setValue(this.value);
    this._typeChangesSubscription$ = this.type.valueChanges.subscribe(
      (data) => {
        this.updateType.emit(data);
      }
    );
  }

  ngOnDestroy(): void {
    this._typeChangesSubscription$?.unsubscribe();
  }
}
