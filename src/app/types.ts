export type QueryArgs = {
  name?: string;
  type?: string;
  port?: string;
  offset?: number;
  limit?: number;
};

export type Ship = {
  id: string;
  name: string;
  type: string;
  home_port: string;
};

export type ShipName = {
  name: string;
};

export type ShipNamesQuery = {
  ships: ShipName[];
};

export type ShipsQuery = {
  ships: Ship[];
};

export type TotalCountQuery = {
  shipsResult: {
    result: {
      totalCount: number;
    };
  };
};

export type ExtendedShip = {
  name: string;
  type: string;
  home_port: string;
  model: string;
  year_built: number;
  missions: {
    name: string;
  }[];
};

export type ShipQuery = {
  ship: ExtendedShip;
};

export type Filters = {
  name?: string;
  ports?: string[] | undefined;
  type?: string;
};

export type Page = {
  querysArgs: QueryArgs[];
  size: number;
};

export type ShipRequest = {
  args: QueryArgs;
  count: number;
};

export type ArgsCreatePageBasedOnShipRequest = {
  querysArgs?: QueryArgs[];
  shipRequest: ShipRequest;
  limit: number;
  offset: number;
  size: number;
};

export type ArgsComplementPage = {
  page: Page;
  shipRequest: ShipRequest;
  pageSize: number;
  shipCount: number;
};

export type ArgsCreatePage = {
  shipRequest: ShipRequest;
  pageSize: number;
  shipCount: number;
};
