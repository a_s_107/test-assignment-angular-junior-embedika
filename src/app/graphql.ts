import { gql } from 'apollo-angular';

export const SHIPS_QUERY = gql`
  query Ships(
    $offset: Int
    $limit: Int
    $name: String
    $type: String
    $port: String
  ) {
    ships(
      offset: $offset
      limit: $limit
      find: { name: $name, type: $type, home_port: $port }
    ) {
      id
      name
      type
      home_port
    }
  }
`;

export const TOTAL_COUNT_SHIP_QUERY = gql`
  query TotalCountShip($name: String, $type: String, $port: String) {
    shipsResult(find: { name: $name, type: $type, home_port: $port }) {
      result {
        totalCount
      }
    }
  }
`;

export const SHIP_QUERY = gql`
  query Ship($id: ID!) {
    ship(id: $id) {
      name
      type
      home_port
      model
      year_built
      missions {
        name
      }
    }
  }
`;

export const SHIP_NAMES = gql`
  query ShipNames {
    ships {
      name
    }
  }
`;
