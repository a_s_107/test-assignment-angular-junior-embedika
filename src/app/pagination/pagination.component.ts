import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.css'],
})
export class PaginationComponent implements OnInit {
  @Input() currentPage!: number;
  @Input() pageCount!: number;
  @Output() updatePageShips = new EventEmitter<number>();

  constructor() {}

  ngOnInit(): void {}
}
