import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { ApolloClientOptions, InMemoryCache } from '@apollo/client/core';
import { ReactiveFormsModule } from '@angular/forms';
import { ApolloModule, APOLLO_OPTIONS } from 'apollo-angular';
import { HttpLink } from 'apollo-angular/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ShipListComponent } from './ship-list/ship-list.component';
import { PaginationComponent } from './pagination/pagination.component';
import { SvgIconArrowComponent } from './svg-icon-arrow/svg-icon-arrow.component';
import { ShipCardComponent } from './ship-card/ship-card.component';
import { ShipFilterComponent } from './ship-filter/ship-filter.component';
import { MultiselectDropdownComponent } from './multiselect-dropdown/multiselect-dropdown.component';
import { SearchFilterComponent } from './search-filter/search-filter.component';
import { SingleSelectComponent } from './single-select/single-select.component';

const URI = 'https://api.spacex.land/graphql';

function createApollo(httpLink: HttpLink): ApolloClientOptions<object> {
  return {
    cache: new InMemoryCache(),
    link: httpLink.create({ uri: URI }),
  };
}

@NgModule({
  declarations: [
    AppComponent,
    ShipListComponent,
    PaginationComponent,
    SvgIconArrowComponent,
    ShipCardComponent,
    ShipFilterComponent,
    MultiselectDropdownComponent,
    SearchFilterComponent,
    SingleSelectComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ApolloModule,
    HttpClientModule,
    ReactiveFormsModule,
  ],
  providers: [
    {
      provide: APOLLO_OPTIONS,
      useFactory: createApollo,
      deps: [HttpLink],
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
