import {
  Component,
  ElementRef,
  EventEmitter,
  HostListener,
  Input,
  OnDestroy,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable, Subscription } from 'rxjs';

import { ShipName } from '../types';

@Component({
  selector: 'app-search-filter',
  templateUrl: './search-filter.component.html',
  styleUrls: ['./search-filter.component.css'],
})
export class SearchFilterComponent implements OnInit, OnDestroy {
  @Input() value: string | undefined;
  @Input() options$!: Observable<ShipName[]>;
  @Output() updateName = new EventEmitter<string>();
  @ViewChild('nameSearch') nameSearch!: ElementRef;

  private _queryShipNamesSubscription$!: Subscription;
  private _nameChangeSubscription$!: Subscription;

  shipNames!: ShipName[];
  namesForAutocomplete: ShipName[] | undefined;
  autocomplete: boolean = false;
  activeNameInAutocomplete: string | undefined;
  indexActiveName: number | undefined;
  name = new FormControl('');

  constructor(private _el: ElementRef) {}

  ngOnInit(): void {
    this.name.setValue(this.value);
    this._queryShipNamesSubscription$ = this.options$.subscribe((data) => {
      this.shipNames = data;
    });
    this._nameChangeSubscription$ = this.name.valueChanges.subscribe(
      (result: string) => {
        this.resetAutocomplete();
        this.namesForAutocomplete = this.shipNames.filter((ship) =>
          ship.name.toLocaleLowerCase().includes(result.toLocaleLowerCase())
        );
        this.autocomplete = true;
      }
    );
  }

  @HostListener('document:click', ['$event'])
  onClick(event: Event) {
    if (!this._el.nativeElement.contains(event.target)) {
      this.resetAutocomplete();
    }
  }

  resetAutocomplete(): void {
    this.activeNameInAutocomplete = undefined;
    this.indexActiveName = undefined;
    this.namesForAutocomplete = undefined;
  }

  moveUp(): void {
    if (this.namesForAutocomplete === undefined) {
      return;
    }

    if (this.indexActiveName === undefined) {
      this.indexActiveName = this.namesForAutocomplete.length - 1;
    } else {
      this.indexActiveName =
        this.indexActiveName === 0
          ? this.namesForAutocomplete.length - 1
          : this.indexActiveName - 1;
    }
    this.activeNameInAutocomplete =
      this.namesForAutocomplete[this.indexActiveName].name;
    document
      .getElementById(this.activeNameInAutocomplete)
      ?.scrollIntoView(false);
  }

  moveDown(): void {
    if (this.namesForAutocomplete === undefined) {
      return;
    }

    if (this.indexActiveName === undefined) {
      this.indexActiveName = 0;
    } else {
      this.indexActiveName =
        this.indexActiveName === this.namesForAutocomplete.length - 1
          ? 0
          : this.indexActiveName + 1;
    }
    this.activeNameInAutocomplete =
      this.namesForAutocomplete[this.indexActiveName].name;
    document
      .getElementById(this.activeNameInAutocomplete)
      ?.scrollIntoView(false);
  }

  moveEnter(): void {
    if (this.activeNameInAutocomplete !== undefined) {
      this.name.setValue(this.activeNameInAutocomplete);
    }
    this.resetAutocomplete();
    this.updateName.emit(this.name.value);
  }

  selectName(name: string): void {
    this.name.setValue(name);
    this.resetAutocomplete();
    this.nameSearch.nativeElement.focus();
    this.updateName.emit(this.name.value);
  }

  close(): void {
    this.resetAutocomplete();
  }

  ngOnDestroy(): void {
    this._queryShipNamesSubscription$.unsubscribe();
    this._nameChangeSubscription$.unsubscribe();
  }
}
